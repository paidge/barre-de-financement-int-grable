<?php 
header('Content-type: image/svg+xml');

if(isset($_GET['lang'])) {
	if(empty($_GET['lang'])) $langdetect = true;
	else $lang = $_GET['lang'];
}
require('lib/locales.php');
include('lib/phpqrcode/qrlib.php');

function isDate(&$date, $format){
	$champsDate = date_parse_from_format($format, $date);
	$date = DateTime::createFromFormat($format, $date);
	return checkdate($champsDate['month'], $champsDate['day'], $champsDate['year']);
}

function base64_encode_image ($filename,$filetype) {
    if ($filename) {
        $imgbinary = fread(fopen($filename, "r"), filesize($filename));
        return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
    }
}

// Déclaration des variables
$whitespace = 20;
$origin_x = $whitespace;
$origin_y = 1.5*$whitespace;
$bar_height = 25;
$bar_width = 500;
$svg_width = 2*$whitespace+$bar_width;
$svg_height = 180;
$icon_size = 25;
$stats_y = $icon_size + $whitespace;
$api_node = 'g1.duniter.fr';
$format_pubkey = '#^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$#';
$units = ['quantitative','relative'];
date_default_timezone_set('Europe/Paris');
$today = new DateTime();
$format = 'Y-m-d';
$display_qrcode = (!empty($_GET['display_qrcode']));
$titre = (!empty($_GET['title'])) ? $_GET['title'] : utf8_decode(tr('title_default'));
$node = (!empty($_GET['node'])) ? $_GET['node'] : 'g1.duniter.org';
$font_color = (!empty($_GET['font_color'])) ? '#' . $_GET['font_color'] : '#212529';
$background_color = (!empty($_GET['background_color'])) ? '#' . $_GET['background_color'] : '#FFF';
$border_color = (!empty($_GET['border_color'])) ? '#' . $_GET['border_color'] : '#343a40';
$progress_color = (!empty($_GET['progress_color'])) ? '#' . $_GET['progress_color'] : '#ffc107';
$logo = (!empty($_GET['logo'])) ? $_GET['logo'] : 'no-logo';
$logo = 'img/logos/svg/' . $logo . '.svg';
if (!file_exists($logo)){$logo='no-logo';}
if ($logo != 'no-logo'){
	$largeur_logo = 150;
	$svg_width += $largeur_logo + $whitespace;
	$origin_x += $largeur_logo + $whitespace;
}
$bar_x = $origin_x;
$bar_y = $origin_y + $whitespace;
$bar_border = 1;

// Vérification des dates et calcul du nombre de jours entre la date du jour et la date de fin
if (!empty($_GET['start_date'])){
	$start_date = $_GET['start_date'];
	$nb_col = 3;
	if (!isDate($start_date, $format)){
		echo '<div>'.tr('error_date_start_invalid').'</div>';
		exit;
	}
	if (!empty($_GET['end_date'])){
		if ($_GET['end_date'] !=0){
			$end_date = $_GET['end_date'];
			if (!isDate($end_date, $format)){
				echo '<div>'.tr('error_date_end_invalid').'</div>';
				exit;
			}elseif ($end_date < $start_date) {
				echo '<div>'.tr('error_date_end_invalid').'</div>';
				exit;
			}
		}
	}
}else{
	// Si pas de start_date, on prend le mois courant
	$start_date = new DateTime('first day of this month');
	$end_date = new DateTime('last day of this month');
}
$start_date->setTime(0,0,0);
if (isset($end_date)){
	$nb_col = 4;
	if ($end_date < $today){
		$days_left = 0;
	}else{
		$dteDiff  = $end_date->diff($today);
		$days_left = $dteDiff->format('%a');
	}
}
$col_width = $bar_width/$nb_col;
$icon_x = $col_width/2-$icon_size/2;

// Vérification du format de la pubkey
if (!empty($_GET['pubkey'])){
	if (preg_match($format_pubkey, $_GET['pubkey'])){
		$pubkey = $_GET['pubkey'];
		$contribution_url = 'https://' . $api_node . '/api/#/v1/payment/' . $pubkey . '?amount=10|20|50|100|1000&amp;comment=don&amp;redirect_url=https%3A%252F%252F' . $api_node . '&amp;cancel_url=https%3A%252F%252F' . $api_node;
		$display_pubkey = (!empty($_GET['display_pubkey']));
		if ($display_pubkey){
			$svg_height += $whitespace;
		}
		// Génération du QRcode
		if ($display_qrcode){
			$qrcode_path = 'img/qrcodes/' . $pubkey . '.svg';
			if (!file_exists($qrcode_path)){
				QRcode::svg($pubkey, $qrcode_path);
			}
			$qrcode_width = 111;
			$svg_width += $qrcode_width + $whitespace;
		}
	}
	else {
		echo '<div>'.tr('error_pubkey_invalid').'</div>';
		exit;
	}
}else{
	echo '<div>'.tr('error_pubkey_missing').'</div>';
	exit;
}

// Vérification du format de la valeur cible à atteindre
if (!empty($_GET['target'])){
	$target = (int)$_GET['target'];
	if (!is_int($target)){
		echo '<div>'.tr('error_target_not_int').'</div>';
		exit;
	}elseif ($target<=0){
		echo '<div>'.tr('error_target_negative').'</div>';
		exit;
	}
}else{
	echo '<div>'.tr('error_target_missing').'</div>';
	exit;
}

// Récupération des transactions entrantes entre la date de début et la date du jour
$start_timestamp = $start_date->getTimestamp();
$today_timestamp = $today->getTimestamp();
$url_json = 'https://' . $node . '/tx/history/' . $pubkey . '/times/' . $start_timestamp . '/' . $today_timestamp;
$json = @file_get_contents($url_json);
if ($json === false){
	echo '<div>'.tr('error_connect_node').'</div>';
	exit;
}
$json = json_decode($json);
$transactions = $json->history->received;
$total = 0;
$donneurs = [];
$tmp_timestamp = $start_timestamp;
$array_final = [];
foreach ($transactions as $transaction){
	$donneur = $transaction->issuers[0];
	if ($donneur != $pubkey){
		if(!in_array($donneur, $donneurs)){
			array_push($donneurs, $donneur);
		}
		$outputs = $transaction->outputs;
		foreach ($outputs as $output){
			if (strstr($output,$pubkey)){
				$timestamp = $transaction->blockstampTime;
				$output = explode(':', $output);
				$montant_transaction = $output[0]/100;
				if ($timestamp != $tmp_timestamp){
					array_push($array_final, ['t'=>$tmp_timestamp*1000, 'y'=>(string) $total]);
				}
				$total += $montant_transaction;
				$tmp_timestamp = $timestamp;
			}
		}
	}
}
$donors = count($donneurs);

// Vérification de l'unité
$unit = (!empty($_GET['unit'])) 
	? ((!in_array($_GET['unit'], $units)) ? 'quantitative' : $_GET['unit'])
	: 'quantitative';
	
// Si l'unité est relative
if ($unit == 'relative'){
	// On récupère le dernier block qui contient le DU
	$url_json = 'https://' . $node . '/blockchain/with/ud';
	$json = file_get_contents($url_json);
	$json = json_decode($json);
	$last_block_with_ud = end($json->result->blocks);
	
	// Puis on récupère le montant du DU pour mettre à jour les données
	$url_json = 'https://' . $node . '/blockchain/block/' . $last_block_with_ud;
	$json = file_get_contents($url_json);
	$json = json_decode($json);
	$ud = $json->dividend/100;
	$total = round($total/$ud);
}
$percentage = round($total/$target,2);
$percentage_bar = ($percentage>1) ? 1 : $percentage;
$bar_innerfill = $percentage_bar*($bar_width - 2*$bar_border);
?>
<?xml version="1.0" encoding="utf-8"?>
<svg width="<?php echo $svg_width; ?>" height="<?php echo $svg_height; ?>" style="fill:<?php echo $font_color; ?>;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<rect
		x="<?php echo $bar_border; ?>"
		y="<?php echo $bar_border; ?>"
		width="<?php echo $svg_width-2*$bar_border; ?>"
		height="<?php echo $svg_height-2*$bar_border; ?>"
		style="fill:<?php echo $background_color; ?>;stroke:<?php echo $border_color; ?>;stroke-width:<?php echo $bar_border; ?>;"/>
	<rect
		x="<?php echo 4*$bar_border; ?>"
		y="<?php echo 4*$bar_border; ?>"
		width="<?php echo $svg_width-8*$bar_border; ?>"
		height="<?php echo $svg_height-8*$bar_border; ?>"
		style="fill:<?php echo $background_color; ?>;stroke:<?php echo $border_color; ?>;stroke-width:<?php echo $bar_border; ?>;"/>
	<?php if ($logo != 'no-logo'){ ?>
	<image xlink:href="<?php echo $logo; ?>" x="<?php echo $whitespace; ?>" y="<?php echo ($svg_height-$largeur_logo)/2; ?>" width="<?php echo $largeur_logo; ?>" height="<?php echo $largeur_logo; ?>" />
	<?php } ?>
	<g style="font-family:sans-serif;">
		<text
			style="font-size:1.5rem;"
			x="<?php echo $origin_x; ?>"
			y="<?php echo $origin_y; ?>"><?php echo $titre; ?></text>
		<?php if ($display_pubkey){ ?>
		<text
			style="font-size:.8rem;"
			x="<?php echo $origin_x; ?>"
			y="<?php echo $origin_y + $whitespace; ?>">Pubkey : <?php echo $pubkey; ?></text>
		<?php $bar_y += $whitespace;} ?>
		<rect
			style="fill:#e9ecef;stroke:<?php echo $border_color; ?>;stroke-width:<?php echo $bar_border; ?>;"
			width="<?php echo $bar_width; ?>"
			height="<?php echo $bar_height; ?>"
			x="<?php echo $origin_x; ?>"
			y="<?php echo $bar_y; ?>" />
		<svg
			x="<?php echo $bar_x + $bar_border; ?>"
			y="<?php echo $bar_y + $bar_border; ?>"
			width="<?php echo $bar_innerfill; ?>"
			height="<?php echo $bar_height - 2*$bar_border; ?>">
			<rect style="fill:<?php echo $progress_color; ?>;" width="100%" height="100%"/>
			<?php if ($percentage>0.07){?>
			<text
				style="font-size:1rem;"
				x="50%" y="50%" dominant-baseline="central" text-anchor="middle"><?php echo $percentage*100 . '%'; ?></text>
			<?php } ?>
		</svg>
	</g>
	<svg x="<?php echo $origin_x; ?>" y="<?php echo $bar_y + $bar_height + $whitespace; ?>" width="<?php echo $bar_width; ?>" height="<?php echo $svg_height - $bar_y - $bar_height - $whitespace; ?>">
		<svg width="<?php echo $col_width; ?>" height="100%">
			<svg x="<?php echo $icon_x; ?>" viewBox="0 0 544 512" width="<?php echo $icon_size; ?>" height="<?php echo $icon_size; ?>">
				<path d="M527.79 288H290.5l158.03 158.03c6.04 6.04 15.98 6.53 22.19.68 38.7-36.46 65.32-85.61 73.13-140.86 1.34-9.46-6.51-17.85-16.06-17.85zm-15.83-64.8C503.72 103.74 408.26 8.28 288.8.04 279.68-.59 272 7.1 272 16.24V240h223.77c9.14 0 16.82-7.68 16.19-16.8zM224 288V50.71c0-9.55-8.39-17.4-17.84-16.06C86.99 51.49-4.1 155.6.14 280.37 4.5 408.51 114.83 513.59 243.03 511.98c50.4-.63 96.97-16.87 135.26-44.03 7.9-5.6 8.42-17.23 1.57-24.08L224 288z"/>
			</svg>
			<text style="font-size:1.2rem;" x="50%" y="<?php echo $stats_y; ?>" dominant-baseline="central" text-anchor="middle"><?php echo $percentage*100 . '%'; ?></text>
			<text style="font-size:.9rem;" x="50%" y="<?php echo $stats_y+$whitespace; ?>" dominant-baseline="central" text-anchor="middle"><?php echo tr('label_reached'); ?></text>
		</svg>
		<svg x="<?php echo $col_width; ?>" width="<?php echo $col_width; ?>">
			<svg x="<?php echo $icon_x; ?>" viewBox="0 0 448 512" width="<?php echo $icon_size; ?>" height="<?php echo $icon_size; ?>">
				<path d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"/>
			</svg>
			<text style="font-size:1.2rem;" x="50%" y="<?php echo $stats_y; ?>" dominant-baseline="central" text-anchor="middle"><?php echo $donors; ?></text>
			<text style="font-size:.9rem;" x="50%" y="<?php echo $stats_y+$whitespace; ?>" dominant-baseline="central" text-anchor="middle"><?php echo tr('label_donators'); ?></text>
		</svg>
		<svg x="<?php echo 2*$col_width; ?>" width="<?php echo $col_width; ?>">
			<svg x="<?php echo $icon_x; ?>" viewBox="0 0 640 512" width="<?php echo $icon_size; ?>" height="<?php echo $icon_size; ?>">
				<path d="M320 144c-53.02 0-96 50.14-96 112 0 61.85 42.98 112 96 112 53 0 96-50.13 96-112 0-61.86-42.98-112-96-112zm40 168c0 4.42-3.58 8-8 8h-64c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h16v-55.44l-.47.31a7.992 7.992 0 0 1-11.09-2.22l-8.88-13.31a7.992 7.992 0 0 1 2.22-11.09l15.33-10.22a23.99 23.99 0 0 1 13.31-4.03H328c4.42 0 8 3.58 8 8v88h16c4.42 0 8 3.58 8 8v16zM608 64H32C14.33 64 0 78.33 0 96v320c0 17.67 14.33 32 32 32h576c17.67 0 32-14.33 32-32V96c0-17.67-14.33-32-32-32zm-16 272c-35.35 0-64 28.65-64 64H112c0-35.35-28.65-64-64-64V176c35.35 0 64-28.65 64-64h416c0 35.35 28.65 64 64 64v160z"/>
			</svg>
			<text style="font-size:1.2rem;" x="50%" y="<?php echo $stats_y; ?>" dominant-baseline="central" text-anchor="middle"><?php echo $total; if ($unit == 'relative'){echo ' '.tr('ud');} ?>Ğ1</text>
			<text style="font-size:.9rem;" x="50%" y="<?php echo $stats_y+$whitespace; ?>" dominant-baseline="central" text-anchor="middle"><?php $unit = ($unit=='relative') ? tr('ud').'Ğ1' : 'Ğ1'; echo tr('label_ontotal_short', array('total'=>$target, 'unit'=>($unit))); ?></text>
		</svg>
		<?php if ($nb_col==4){ ?>
		<svg x="<?php echo 3*$col_width; ?>" width="<?php echo $col_width; ?>">
			<svg x="<?php echo $icon_x; ?>" viewBox="0 0 448 512" width="<?php echo $icon_size; ?>" height="<?php echo $icon_size; ?>">
				<path d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"/>
			</svg>
			<text style="font-size:1.2rem;" x="50%" y="<?php echo $stats_y; ?>" dominant-baseline="central" text-anchor="middle"><?php echo $days_left; ?></text>
			<text style="font-size:.9rem;" x="50%" y="<?php echo $stats_y+$whitespace; ?>" dominant-baseline="central" text-anchor="middle"><?php echo tr('label_remaining_d'); ?></text>
		</svg>
		<?php } ?>
	</svg>
	<?php if ($display_qrcode){ ?>
	<image xlink:href="<?php echo $qrcode_path; ?>" x="<?php echo $bar_x + $bar_width + $whitespace; ?>" y="<?php echo ($svg_height-$qrcode_width)/2; ?>" width="<?php echo $qrcode_width; ?>" height="<?php echo $qrcode_width; ?>" />
	<?php } ?>
</svg>