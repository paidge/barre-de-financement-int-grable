<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="styles/w3.css"/>
	<link rel="stylesheet" href="styles/font_awesome.min.css"/>
	<link rel="stylesheet" href="styles/gh-fork-ribbon.min.css" />
	<link rel="stylesheet" href="styles/generator.css"/>
	<title>Génération de votre barre de financement</title>
</head>
<body>
<a class="github-fork-ribbon" href="https://git.duniter.org/paidge/barre-de-financement-int-grable" data-ribbon="Fork me on Gitlab" title="Fork me on Duniter's GitLab">Fork me on Duniter's GitLab</a>
<header>
	<div class="w3-panel w3-padding-16 w3-display-middle w3-center w3-theme-d2">
		<h1 class="w3-jumbo">Générez votre barre de financement</h1>
		<h2>En monnaie libre Ğ1</h2>
		<p><a href="#content" id="smooth-scroll" class="w3-btn w3-theme-l5 w3-padding-large w3-large w3-margin-top w3-hover-theme">Commencer</a></p>
	</div>
</header>
<section id="content" class="w3-padding-32 w3-container w3-theme-l1">
	<div class="w3-content">
		<form class="w3-container">
			<fieldset class="w3-theme-l5">
				<legend class="w3-theme-d5 w3-padding-small w3-xlarge">Paramètres obligatoires</legend>
				<p class="field">
					<label for="type">Type d'intégration&nbsp;:</label>
					<select id="type" name="type" class="w3-select w3-border" required>
						<option value="iframe" selected>Iframe</option>
						<option value="png">Image PNG</option>
						<option value="svg">Image SVG</option>
					</select>
				</p>
				<p class="field">
					<label for="pubkey">Clé publique&nbsp;:</label>
					<input id="pubkey" name="pubkey" type="text" class="w3-input w3-border w3-animate-input" required placeholder="27b1j7BPssdjbXmGNMYU2JJrRotqrZMruu5p5AWowUEy" pattern="^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{43,44}$" />
				</p>
				<p class="field">
					<label for="target">Montant cible à atteindre&nbsp;:</label>
					<input id="target" name="target" type="number" class="w3-input w3-border" required placeholder="1000" min="0" />
				</p>
			</fieldset>
			<fieldset class="w3-theme-l5">
				<legend class="w3-theme-d5 w3-padding-small w3-xlarge">Paramètres facultatifs</legend>
				<p class="field">
					<label for="title">Titre&nbsp;:</label>
					<input id="title" name="title" type="text" class="w3-input w3-border w3-animate-input" placeholder="Financement participatif en monnaie libre" />
				</p>
				<p class="field">
					<label for="unit">Unité&nbsp;:</label>
					<select id="unit" name="unit" class="w3-select w3-border">
						<option value="quantitative" selected>quantitatif (Junes)</option>
						<option value="relative">relatif (DU)</option>
					</select>
				</p>
				<p class="field">
					<label for="lang">Langue&nbsp;:</label>
					<select id="lang" name="lang" class="w3-select w3-border">
						<option value="fr" selected>français</option>
						<option value="en">anglais</option>
						<option value="eo">espéranto</option>
					</select>
				</p>
				<p class="field">
					<label for="node">Nœuds Duniter (séparés par un espace)&nbsp;:</label>
					<input id="node" name="node" type="text" class="w3-input w3-border w3-animate-input" placeholder="https://g1.duniter.org" pattern="^(https?:\/\/(\[[a-fA-F0-9:]+\]|(\w|[._-])+[^.])(:\d{1,5})?(\s+|$))+$" />
				</p>
				<fieldset class="w3-theme-l5">
					<legend class="w3-theme-d5 w3-padding-small w3-xlarge">Période de financement</legend>
					<p class="field">
						<label for="period">Période de financement&nbsp;:</label>
						<select id="period" name="period" class="w3-select w3-border">
							<option value="current-monh" selected>Mois courant</option>
							<option value="one-date">A partir d'une date</option>
							<option value="two-dates">Entre deux dates</option>
						</select>
					</p>
					<p id="p-start_date" class="field w3-hide">
						<label for="start_date">Date de début&nbsp;:</label>
						<input id="start_date" name="start_date" type="date" class="w3-input w3-border" />
					</p>
					<p id="p-end_date" class="field w3-hide">
						<label for="end_date">Date de fin&nbsp;:</label>
						<input id="end_date" name="end_date" type="date" class="w3-input w3-border" />
					</p>
				</fieldset>
				<fieldset class="w3-theme-l5">
					<legend class="w3-theme-d5 w3-padding-small w3-xlarge">Options d'affichage</legend>
					<p class="field">
						<label for="display_pubkey">Affichage de la clé publique&nbsp;:</label>
						<input id="display_pubkey" name="display_pubkey" type="checkbox" class="w3-check" />
					</p>
					<p class="field">
						<label for="display_qrcode">Affichage du QRcode&nbsp;:</label>
						<input id="display_qrcode" name="display_qrcode" type="checkbox" class="w3-check" />
					</p>
					<p id="p-display_button" class="field">
						<label for="display_button">Affichage du bouton API Cesium&nbsp;:</label>
						<input id="display_button" name="display_button" type="checkbox" class="w3-check" />
					</p>
					<p id="p-display_graph" class="field">
						<label for="display_graph">Affichage du graphique&nbsp;:</label>
						<input id="display_graph" name="display_graph" type="checkbox" class="w3-check" />
					</p>
					<p id="p-logo" class="field w3-hide">
						<label for="logo">Logo&nbsp;:</label>
						<select id="logo" name="logo" class="w3-select w3-border">
							<option value="no-logo" selected>Aucun</option>
							<option value="cesium">Cesium</option>
							<option value="duniter">Duniter</option>
							<option value="dunitrust">Dunitrust</option>
							<option value="junes">Junes</option>
							<option value="sakia">Sakia</option>
							<option value="silkaj">Silkaj</option>
						</select>
					</p>
				</fieldset>
				<fieldset class="w3-theme-l5">
					<legend class="w3-theme-d5 w3-padding-small w3-xlarge">Personnalisation des couleurs</legend>
					<p class="field">
						<label for="background_color">Couleur de fond&nbsp;:</label>
						<input id="background_color" name="background_color" type="color" value="#ffffff" />
					</p>
					<p class="field">
						<label for="font_color">Couleur de la police&nbsp;:</label>
						<input id="font_color" name="font_color" type="color" value="#212529" />
					</p>
					<p class="field">
						<label for="progress_color">Couleur de la barre de progression&nbsp;:</label>
						<input id="progress_color" name="progress_color" type="color" value="#ffc107" />
					</p>
					<p class="field">
						<label for="border_color">Couleur des bordures&nbsp;:</label>
						<input id="border_color" name="border_color" type="color" value="#343a40" />
					</p>
				</fieldset>
			</fieldset>
			<div class="w3-center">
				<button id="submit" class="w3-btn w3-theme-l5 w3-padding-large w3-large w3-margin-top w3-hover-theme">Générer</button>
			</div>
		</form>
	</div>
</section>
<section id="display_result" class="w3-padding-32 w3-container w3-theme-d1">
	<div class="w3-content">
		<h2>Résultat</h2>
		<p>Copier-coller le code suivant pour l'intégrer sur votre site web/blog/forum&nbsp;:</p>
		<textarea id="result" onclick="select();" onfocus="select();" readonly></textarea>
		<div id="buttons" class="w3-bar w3-center w3-hide">
			<div class="tooltip"><button id="copy" class="w3-btn w3-theme-l5 w3-padding-large w3-large w3-margin-top w3-hover-theme" onclick="copyToClipboard('#result')">Copier</button>
			<span class="tooltiptext">Copié&#8239;!</span></div>
			<button id="reset" class="w3-btn w3-theme-l5 w3-padding-large w3-large w3-margin-top w3-hover-theme">Réinitialiser</button>
		</div>
		<h2 id="preview_label" class="w3-hide">Prévisualisation</h2>
		<div id="preview" class="w3-margin-top w3-center">
		</div>
	</div>
</section>
<div id="back-to-top" class="w3-hover-theme">
	<i class="fa fa-angle-double-up" aria-hidden="true"></i>
</div>
<footer class="w3-container w3-padding-16 w3-center w3-theme-d5">
	<p>Code source disponible sur <a href="https://git.duniter.org/paidge/barre-de-financement-int-grable" target="_blank">le GitLab Duniter</a></p>
	<p>Développé par <a href="https://g1.duniter.fr/#/app/wot/27b1j7BPssdjbXmGNMYU2JJrRotqrZMruu5p5AWowUEy/" target="_blank">Paidge</a></p>
	<div class="w3-xlarge">
		<a href="https://www.facebook.com/pjchancellier" target="_blank" title="Facebook" aria-label="Facebook"><i class="fab fa-facebook-f w3-margin-right"></i></a>
		<a href="https://diaspora.normandie-libre.fr/people/97f6cd801bcf01365ebe002564b8841f" target="_blank" title="Diaspora" aria-label="Diaspora"><i class="fab fa-diaspora w3-margin-right"></i></a>
		<a href="https://www.youtube.com/watch?v=SjoYIz_3JLI&list=PLmKEBjWrttOu93a92v62EWnjcs_fX6I7C" target="_blank" title="Youtube" aria-label="Youtube"><i class="fab fa-youtube w3-margin-right"></i></a>
		<a href="https://normandie-libre.fr" target="_blank" title="Normandie Libre" aria-label="Normandie Libre"><i class="fas fa-globe"></i></a>
	</div>
</footer>
<script>
</script>
<script src="lib/js/jquery-3.4.1.min.js"></script>
<script src="lib/js/generate.js"></script>
<script>
function copyToClipboard(element) {
	$(element).select();
	document.execCommand("copy");
	$('.tooltip').addClass('tooltip_display');
	setTimeout(function(){$('.tooltip').removeClass('tooltip_display');}, 600)
}
</script>
</body>
</html>
